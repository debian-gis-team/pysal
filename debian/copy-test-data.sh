#!/bin/sh

set -e

version="$1"
interpreter="$2"
dir="$3"
destdir="$4"
home_dir="$5"
build_dir="$6"
install_dir="$7"
package="$8"

cp -rv ${dir}/pysal/lib/cg/tests/data                  ${build_dir}/pysal/lib/cg/tests/
cp -rv ${dir}/pysal/model/mgwr/tests                   ${build_dir}/pysal/model/mgwr/
cp -rv ${dir}/pysal/model/spvcm/both_levels/tests/data ${build_dir}/pysal/model/spvcm/both_levels/tests/
cp -rv ${dir}/pysal/model/spvcm/lower_level/tests/data ${build_dir}/pysal/model/spvcm/lower_level/tests/
cp -rv ${dir}/pysal/model/spvcm/upper_level/tests/data ${build_dir}/pysal/model/spvcm/upper_level/tests/
cp -rv ${dir}/pysal/model/spvcm/tests/data             ${build_dir}/pysal/model/spvcm/tests/
cp -rv ${dir}/pysal/viz/mapclassify/datasets/calemp    ${build_dir}/pysal/viz/mapclassify/datasets/
